module private

go 1.15

require github.com/zeromicro/go-zero v1.4.1

require (
	github.com/Masterminds/squirrel v1.5.3
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/mattn/go-colorable v0.1.12 // indirect
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/net v0.0.0-20220920203100-d0c6ba3f52d9 // indirect
	golang.org/x/sys v0.0.0-20220919091848-fb04ddd9f9c8 // indirect
	google.golang.org/genproto v0.0.0-20220920201722-2b89144ce006 // indirect

)
